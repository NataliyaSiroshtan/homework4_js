// Объект — (совокупность связанных данных и функциональных возможностей) это набор свойств, где каждое свойство состоит из имени и значения (которые можно менять, добавлять и убирать), а также функции для работы с этими значениями. Эти функции, которые являются свойством объекта, называют методами этого объекта.
//Экранирование - это когда определенные символы или последовательности символов в тексте имеют особое значение, то должны быть правила, определяющие, как разрешить ситуации, когда эти символы должны использоваться без привлечения своего особого значения.Поскольку компьютер не очень справляется с неоднозначностями, то что-то в итоге может дать непредвиденный результат, если мы не устраним неоднозначности. Другими словами, когда необходимо использовать какой-то символ в качестве «обычного символа языка» (распознается как текст, а не часть кода). Например, нам нужно в строке указать кавычки и объяснить интерпретатору, что некоторые кавычки он должен воспринимать иначе. Они не должны означать "начало строки" или "конец строки", они должны означать "символ кавычек" (только когда это кавычки одинакового типа). Это и называется экранированием. Чтобы добавить символ экранирования необходтмо указать обратный слеш \ перед символом, и символ "изолируется" от своей специфической роли и превратится в обычный знак в строке. Все спецсимволы начинаются с обратного слеша \ — «символа экранирования».

function createNewUser() {
  let firstName = prompt("Введите Ваше имя");
  let lastName = prompt("Введите фамилию");
  let birthday = prompt("Введите дату рождения", "dd.mm.yyyy");
  const newUser = {
    firstName,
    lastName,
    birthday,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6)
      );
    },
    getAge() {
      let dayUser = this.birthday.slice(0, 2);
      let monthUser = this.birthday.slice(3, 5);
      let yearUser = this.birthday.slice(6);
      let today = new Date();
      let day = today.getDate();
      let month = today.getMonth() + 1;
      let year = today.getFullYear();
      let userAge = year - yearUser;
      if (month < monthUser) {
        return userAge - 1;
      } else if (month > monthUser) {
        return userAge;
      } else {
        if (day < dayUser) {
          return userAge - 1;
        } else {
          return userAge;
        }
      }
    },
  };
  return newUser;
}

let user = createNewUser();
console.log(user);
let login = user.getLogin();
console.log(login);
let age = user.getAge();
console.log(age);
let password = user.getPassword();
console.log(password);
